# CI Integration Documentation Template

**NOTE:** In order to increase modularity of the documentation and keep the README within a reasonable lenght, **sections and sub-sections can link to nested documentation files** in the relative sub-folder.

## Table of contents

- Description
    - Architecture overview
    - Relation with SOFIE
    - Main Concepts (OPTIONAL)
    - Key Technologies
- Usage
    - Prerequisites
    - Installation
    - Configuration (OPTIONAL)
    - Execution
- Testing
    - Prerequisites
    - Executing the tests
    - Evaluating the results
- Known/Open Issues (OPTIONAL)
- Future Work (OPTIONAL)
- Release Notes
- Contact Info
- License
- Component-specific info (OPTIONAL)

## Description

Briefly describe what the component does (1-2 paragraphs) and what the component can be used for, with some examples of its applications (1-2 paragraphs).

### Architecture Overview

A high-level view of how the system is structured, including visual assets (e.g., images).

In particular, this section should also contain class/sequence diagrams along with the description of the different sub-components involved.

### Relation with SOFIE

How the project relates to SOFIE and what are the key components that allow such integration.

### Main Concepts (OPTIONAL)

Any specific topic mentioned in the previous parts or that will be mentioned in the next sections, and that might need a more in-depth discussion, can be added here each as a sub-section.

### Key Technologies

A list of the technologies used, and for which purpose/sub-component.

## Usage

If the system offers several features, an example of API and how to use such features might be very insightful and helpful. Furthermore, a description of how the component in the example possibly interacts with other compents is needed.

### Prerequisites

A list of all the dependencies needed in order to run the system, and how to fetch and install them.

### Installation

How to install the component(s), e.g. setting up a Python virtual environment or a Node project.

### Configuration (OPTIONAL)

Instruct about any parameters/files that need to be modified (e.g., environment variables, paths) according to specific needs, before executing the environment. Any additional elements that need to be built before execution (e.g., containers, virtual environments, etc...) must be listed.

### Execution

An explanation about how to run the environment, what parameters can be used upon launch, and how such parameters affect the execution (e.g., development vs staging environment).

## Testing

### Prerequisites

A list of all the dependencies needed in order to run the test cases (e.g., a specific testing suite), and how to fetch and install them.

### Running the tests

Explain how to run any automated tests, and what such tests cover, in a high-level manner.

### Evaluating the results

Provide information about where the tests results are saved and in which format. Any additional information needed to properly evaluate the test results must be added.

## Known/Open Issues (OPTIONAL)

An explanation of any known issues/bugs/vulnerabilities that have not yet been fixed, along with a brief description of the status of the issue (e.g., open/fixing).

## Future Work (OPTIONAL)

What is the roadmap until the end of the project (and perhaps after that). What features/changes are going to be implemented.

## Release Notes

An example could be (taken from the Interledger repository):

### 2020-07-31

#### Added

- Support for Hyperledger Fabric ledger
- Example of using Hash Time Locked Contracts (HTLCs) with Interledger
- Support for passwords and PoA middleware for Ethereum

#### Changed

- Performance improvements and opmitizations for the Interledger core

#### Security

- Update Truffle dependencies due to vulnerability in lodash

## Contact info

Contact information of the person or people to contact in case of any issues with the component repository.

## License

License details as agreed for the SOFIE releases.

***

## _Pilot-specific info (OPTIONAL)_

Depending on the standards and policies defining each component, additional sections, including _Contributing_, _Authors_, or _Acknowledgments_, can be added at the end of this README.